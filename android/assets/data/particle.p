Untitled
- Delay -
active: false
- Duration - 
lowMin: 700.0
lowMax: 700.0
- Count - 
min: 0
max: 10
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 250.0
highMax: 250.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 1000.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 1.0
scaling2: 0.3
timelineCount: 3
timeline0: 0.0
timeline1: 0.66
timeline2: 1.0
- Life Offset - 
active: false
- X Offset - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 4.0
highMax: 32.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 150.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 360.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 0.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.5
timeline2: 1.0
- Rotation - 
active: false
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 15
colors0: 1.0
colors1: 0.12156863
colors2: 0.047058824
colors3: 1.0
colors4: 0.5803922
colors5: 0.24313726
colors6: 1.0
colors7: 0.7921569
colors8: 0.24313726
colors9: 1.0
colors10: 0.94509804
colors11: 0.047058824
colors12: 1.0
colors13: 0.42745098
colors14: 0.3764706
timelineCount: 5
timeline0: 0.0
timeline1: 0.37288135
timeline2: 0.48022598
timeline3: 0.80225986
timeline4: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.7022472
scaling1: 0.92134833
scaling2: 0.75842696
scaling3: 0.40449437
timelineCount: 4
timeline0: 0.0
timeline1: 0.3516949
timeline2: 0.70974576
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
- Image Path -
C:\Users\Mete\Desktop\LibGDX_GameAssets\Resized Assets\particle.png
