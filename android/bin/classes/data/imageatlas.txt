imageatlas.png
format: RGBA8888
filter: Linear,Linear
repeat: none
img_btn_genereal_pressed
  rotate: false
  xy: 754, 623
  size: 250, 80
  orig: 250, 80
  offset: 0, 0
  index: -1
img_obj_trns_black_2_
  rotate: false
  xy: 754, 704
  size: 250, 80
  orig: 250, 80
  offset: 0, 0
  index: -1
img_btn_genereal
  rotate: false
  xy: 654, 542
  size: 250, 80
  orig: 250, 80
  offset: 0, 0
  index: -1
img_btn_home
  rotate: false
  xy: 805, 785
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
img_obj_actor_3_
  rotate: false
  xy: 503, 843
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
img_obj_mtxlogo
  rotate: false
  xy: 1, 843
  size: 350, 150
  orig: 350, 150
  offset: 0, 0
  index: -1
img_btn_home_pressed
  rotate: false
  xy: 654, 785
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
img_obj_techo_disc
  rotate: false
  xy: 302, 542
  size: 300, 300
  orig: 300, 300
  offset: 0, 0
  index: -1
img_bg_1_
  rotate: false
  xy: 1, 1
  size: 960, 540
  orig: 960, 540
  offset: 0, 0
  index: -1
img_obj_actor_2_
  rotate: false
  xy: 603, 623
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
img_obj_actor_1_
  rotate: false
  xy: 352, 843
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
img_obj_heart
  rotate: false
  xy: 1, 542
  size: 300, 300
  orig: 300, 300
  offset: 0, 0
  index: -1
img_obj_trns_black
  rotate: false
  xy: 962, 1
  size: 20, 20
  orig: 20, 20
  offset: 0, 0
  index: -1
