imageatlas_2.png
format: RGBA8888
filter: Linear,Linear
repeat: none
img_obj_heart_2_
  rotate: false
  xy: 1558, 152
  size: 125, 125
  orig: 125, 125
  offset: 0, 0
  index: -1
img_obj_arrow
  rotate: false
  xy: 1, 1955
  size: 125, 70
  orig: 125, 70
  offset: 0, 0
  index: -1
img_obj_heart_1_
  rotate: false
  xy: 1, 1
  size: 600, 600
  orig: 600, 600
  offset: 0, 0
  index: -1
anim_ignite_
  rotate: false
  xy: 1558, 1
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: 1
anim_ignite_
  rotate: false
  xy: 152, 1804
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: 5
anim_fire_
  rotate: false
  xy: 1407, 152
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: 4
img_obj_tech_inner_1_
  rotate: false
  xy: 605, 1
  size: 400, 400
  orig: 400, 400
  offset: 0, 0
  index: -1
anim_bat_fly
  rotate: false
  xy: 127, 1955
  size: 385, 64
  orig: 385, 64
  offset: 0, 0
  index: -1
anim_ignite_
  rotate: false
  xy: 1407, 1
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: 2
anim_fire_
  rotate: false
  xy: 1860, 1
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: 5
anim_ignite_
  rotate: false
  xy: 756, 402
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: 6
anim_fire_
  rotate: false
  xy: 605, 402
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: 1
img_obj_cloud_1_
  rotate: false
  xy: 1006, 1
  size: 400, 300
  orig: 400, 300
  offset: 0, 0
  index: -1
img_obj_snowflake_1_
  rotate: false
  xy: 1684, 152
  size: 100, 100
  orig: 100, 100
  offset: 0, 0
  index: -1
anim_ignite_
  rotate: false
  xy: 454, 1804
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: 3
anim_fire_
  rotate: false
  xy: 1709, 1
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: 6
anim_fire_
  rotate: false
  xy: 1157, 302
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: 2
img_obj_heart_shine_1_
  rotate: false
  xy: 1, 602
  size: 600, 600
  orig: 600, 600
  offset: 0, 0
  index: -1
img_obj_metal_ball
  rotate: false
  xy: 1, 1804
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
anim_ignite_
  rotate: false
  xy: 303, 1804
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: 4
anim_fire_
  rotate: false
  xy: 1006, 302
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: 3
img_obj_tech_outer_1_
  rotate: false
  xy: 1, 1203
  size: 600, 600
  orig: 600, 600
  offset: 0, 0
  index: -1
