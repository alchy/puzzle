package com.juego.control.Rompecabeza;

public class MovimientoLaterales implements Movible{

	private Movible movible;
	private Mesa mesa;
	
	
	
	public MovimientoLaterales(Mesa mesa) {
		// TODO Auto-generated constructor stub
		this.mesa=mesa;
	}
	@Override
	public void setNextMovible(Movible movible) {
		// TODO Auto-generated method stub
		this.movible=movible;
	}

	@Override
	public boolean mover(int pieza) {
		// TODO Auto-generated method stub
	    if (mesa.getX_spc()==mesa.getTam()-1 && mesa.getY_spc()>0 && mesa.getY_spc()<mesa.getTam()-1){
	        if (mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()+1]==pieza){
	          
	           mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()]=mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()+1];
	           mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()+1]=0;

	           
	           mesa.setY_spc(mesa.getY_spc()+1);
	           return true;
	        }
	        else if(mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()-1]==pieza){
	            
	           mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()]=mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()-1];
	           mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()-1]=0;
	           
	           mesa.setY_spc(mesa.getY_spc()-1);
	           return true;
	        }
	        else if (mesa.getPiezas()[mesa.getX_spc()-1][mesa.getY_spc()]==pieza){

	           mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()]=mesa.getPiezas()[mesa.getX_spc()-1][mesa.getY_spc()];
	           mesa.getPiezas()[mesa.getX_spc()-1][mesa.getY_spc()]=0;
	           
	           mesa.setX_spc(mesa.getX_spc()-1);
	           return true;
	        }
	        
	    }
	    else if (mesa.getX_spc()>0 && mesa.getX_spc()<mesa.getTam()-1 && mesa.getY_spc()==0){
	        if (mesa.getPiezas()[mesa.getX_spc()+1][mesa.getY_spc()]==pieza){
	            mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()]=mesa.getPiezas()[mesa.getX_spc()+1][mesa.getY_spc()];
	            mesa.getPiezas()[mesa.getX_spc()+1][mesa.getY_spc()]=0;
	            
	            mesa.setX_spc(mesa.getX_spc()+1);
	            return true;
	        }
	        else if(mesa.getPiezas()[mesa.getX_spc()-1][mesa.getY_spc()]==pieza){
	            mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()]=mesa.getPiezas()[mesa.getX_spc()-1][mesa.getY_spc()];
	            mesa.getPiezas()[mesa.getX_spc()-1][mesa.getY_spc()]=0;
	            
	            mesa.setX_spc(mesa.getX_spc()-1);
	            return true;
	            
	        }
	        else if (mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()+1]==pieza){
	            mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()]=mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()+1];
	            mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()+1]=0;
	            
	            mesa.setY_spc(mesa.getY_spc()+1);
	            return true;
	        }
	    
	    }
	    else if (mesa.getX_spc()==0 && mesa.getY_spc()>0 && mesa.getY_spc()<mesa.getTam()-1){
	        if (mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()-1]==pieza){

	            mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()]=pieza;
	            mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()-1]=0;
	            
	            mesa.setY_spc(mesa.getY_spc()-1);
	            return true;

	        }
	        else if(mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()+1]==pieza){

	            mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()]=pieza;
	            mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()+1]=0;
	            
	            mesa.setY_spc(mesa.getY_spc()+1);
	            return true;
	        }
	        else if(mesa.getPiezas()[mesa.getX_spc()+1][mesa.getY_spc()]==pieza){
	            
	            mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()]=pieza;
	            mesa.getPiezas()[mesa.getX_spc()+1][mesa.getY_spc()]=0;
	            mesa.setX_spc(mesa.getX_spc()+1);
	            
	            return true;
	        }
	    
	    }	    
		
	    else if (mesa.getY_spc()==mesa.getTam()-1 && mesa.getX_spc()>0 && mesa.getX_spc()<mesa.getTam()-1){

	        if (mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()-1]==pieza){
	            mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()]=pieza;
	            mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()-1]=0;
	            
	            mesa.setY_spc(mesa.getY_spc()-1);
	            return true;
	    
	        }
	        else if(mesa.getPiezas()[mesa.getX_spc()+1][mesa.getY_spc()]==pieza){
	            mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()]=pieza;
	            mesa.getPiezas()[mesa.getX_spc()+1][mesa.getY_spc()]=0;
	            
	            mesa.setX_spc(mesa.getX_spc()+1);
	            return true;
	            
	        }
	        else if(mesa.getPiezas()[mesa.getX_spc()-1][mesa.getY_spc()]==pieza){
	            mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()]=pieza;       
	            mesa.getPiezas()[mesa.getX_spc()-1][mesa.getY_spc()]=0;
	            
	            mesa.setX_spc(mesa.getX_spc()-1);
	            return true;
	            

	        }
	    }
	    
	    else return movible.mover(pieza);
	    
	    
	    return false;
	}
	

}
