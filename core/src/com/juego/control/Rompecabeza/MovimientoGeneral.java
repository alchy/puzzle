package com.juego.control.Rompecabeza;

public  class MovimientoGeneral implements Movible  {

	private Movible movible;
	private Mesa mesa;
	public MovimientoGeneral(Mesa mesa) {
		// TODO Auto-generated constructor stub
		this.mesa=mesa;
	}

	@Override
	public void setNextMovible(Movible movible) {
		// TODO Auto-generated method stub
		this.movible=movible;
	}

	@Override
	public boolean mover(int pieza) {
		// TODO Auto-generated method stub
		  
		        if (mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()+1]==pieza){
		            mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()]=pieza;
		            mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()+1]=0;
		            //mesa.getY_spc()++;
		            mesa.setY_spc(mesa.getY_spc()+1);
		            return true;
		            
		            
		        }
		        else if(mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()-1]==pieza){
		            mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()]=pieza;
		            mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()-1]=0;
		            //mesa.getY_spc()--;
		            mesa.setY_spc(mesa.getY_spc()-1);
		            return true;
		            
		            
		        }
		        else if(mesa.getPiezas()[mesa.getX_spc()+1][mesa.getY_spc()]==pieza){
		           mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()]=pieza;
		           mesa.getPiezas()[mesa.getX_spc()+1][mesa.getY_spc()]=0;
		           //mesa.getX_spc()++;
		           mesa.setX_spc(mesa.getX_spc()+1);
		           return true;
		           
		        }
		        else if (mesa.getPiezas()[mesa.getX_spc()-1][mesa.getY_spc()]==pieza){
		           mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()]=pieza;
		           mesa.getPiezas()[mesa.getX_spc()-1][mesa.getY_spc()]=0;
		           //mesa.getX_spc()--;
		           mesa.setX_spc(mesa.getX_spc()-1);
		           return true;
		           
		        }	    
			return false;
		    
	}
	

	
	
}
