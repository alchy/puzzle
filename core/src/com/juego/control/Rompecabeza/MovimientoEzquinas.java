package com.juego.control.Rompecabeza;

public class MovimientoEzquinas implements Movible {

	private Movible movible;
	private Mesa mesa;
	
	public MovimientoEzquinas(Mesa mesa) {
		// TODO Auto-generated constructor stub
		this.mesa=mesa;
	}
	@Override
	public void setNextMovible(Movible movible) {
		// TODO Auto-generated method stub
		this.movible=movible;
	}

	@Override
	public boolean mover(int pieza) {
		// TODO Auto-generated method stub
		
		   if (mesa.getX_spc()==mesa.getTam()-1 && mesa.getY_spc()==mesa.getTam()-1){
		        if (mesa.getPiezas()[mesa.getTam()-2][mesa.getTam()-1]==pieza){
		           mesa.getPiezas()[mesa.getTam()-1][mesa.getTam()-1]=mesa.getPiezas()[mesa.getTam()-2][mesa.getTam()-1];
		           mesa.getPiezas()[mesa.getTam()-2][mesa.getTam()-1]=0; 
		           /*
		            *   x_spc=tam-2;
           				y_spc=tam-1;	
		            * */
		           mesa.setX_spc(mesa.getTam()-2);
		           mesa.setY_spc(mesa.getTam()-1);
		           return true;
		        }
		        else if(mesa.getPiezas()[mesa.getTam()-1][mesa.getTam()-2]==pieza){
		           mesa.getPiezas()[mesa.getTam()-1][mesa.getTam()-1]=mesa.getPiezas()[mesa.getTam()-1][mesa.getTam()-2];
		           mesa.getPiezas()[mesa.getTam()-1][mesa.getTam()-2]=0; 
		          /*
		           *  x_spc=tam-1;
           			  y_spc=tam-2; 
		           * */
		           
		           mesa.setX_spc(mesa.getTam()-1);
		           mesa.setY_spc(mesa.getTam()-2);
		           
		           return true;            
		        }
		    }
		   else if (mesa.getX_spc()==mesa.getTam()-1 && mesa.getY_spc()==0){
		        if (mesa.getPiezas()[mesa.getTam()-1][mesa.getY_spc()+1]==pieza){
		            mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()]=mesa.getPiezas()[mesa.getTam()-1][mesa.getY_spc()+1];
		            mesa.getPiezas()[mesa.getTam()-1][mesa.getY_spc()+1]=0;
		            /*
		             * x_spc=tam-1;
            		   y_spc=y_spc+1;
		             * */
		            
		            mesa.setX_spc(mesa.getTam()-1);
		            mesa.setY_spc(mesa.getY_spc()+1);;
		            return true;
		            
		        }
		        else if (mesa.getPiezas()[mesa.getX_spc()-1][mesa.getY_spc()]==pieza){
		            mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()]=mesa.getPiezas()[mesa.getX_spc()-1][mesa.getY_spc()];
		            mesa.getPiezas()[mesa.getX_spc()-1][mesa.getY_spc()]=0;
		            //x_spc=x_spc-1;
		            mesa.setX_spc(mesa.getX_spc()-1);
		            return true;
		        }
		    }
		    else if (mesa.getX_spc()==0 && mesa.getY_spc()==0){
		        if (mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()+1]==pieza){
		          mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()]=mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()+1];
		          mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()+1]=0;
		          //  y_spc=y_spc+1;
		          mesa.setY_spc(mesa.getY_spc()+1);
		          return true;
		        
		        }
		        else if(mesa.getPiezas()[mesa.getX_spc()+1][mesa.getY_spc()]==pieza){
		          mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()]=mesa.getPiezas()[mesa.getX_spc()+1][mesa.getY_spc()];
		          mesa.getPiezas()[mesa.getX_spc()+1][mesa.getY_spc()]=0;
		          //x_spc=x_spc+1;
		          mesa.setX_spc(mesa.getX_spc()+1);
		          return true;
		        }
		        
		     }	
		    else if(mesa.getX_spc()==0 && mesa.getY_spc()==mesa.getTam()-1){
		        if (mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()-1]==pieza){
		            mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()]=pieza;
		            mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()-1]=0;
		            //y_spc--;
		            mesa.setY_spc(mesa.getY_spc()-1);
		            return true;
		        }
		        else if(mesa.getPiezas()[mesa.getX_spc()+1][mesa.getY_spc()]==pieza){
		            mesa.getPiezas()[mesa.getX_spc()][mesa.getY_spc()]=pieza;
		            mesa.getPiezas()[mesa.getX_spc()+1][mesa.getY_spc()]=0;
		            //x_spc++;
		            mesa.setX_spc(mesa.getX_spc()+1);
		            
		            return true;
		        
		        }
		    
		    }		   
		    else return movible.mover(pieza);
		   
		   
		   return false;
	}



}
