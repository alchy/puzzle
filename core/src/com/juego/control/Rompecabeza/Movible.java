package com.juego.control.Rompecabeza;

 public interface Movible {
	public void setNextMovible(Movible movible);
	public boolean mover(int pieza);
}
