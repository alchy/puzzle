package com.juego.control.Recursos;

public class Nivel {
	private int nivel;
	private int puntuacion;
	private String nombre;
	private boolean estado;
	
	public Nivel() {
		// TODO Auto-generated constructor stub
	}
	public Nivel(int nivel, int puntuacion, String nombre, boolean estado) {
		super();
		this.nivel = nivel;
		this.puntuacion = puntuacion;
		this.nombre = nombre;
		this.estado = estado;
	}
	
	public int getNivel() {
		return nivel;
	}
	public void setNivel(int nivel) {
		this.nivel = nivel;
	}
	public int getPuntuacion() {
		return puntuacion;
	}
	public void setPuntuacion(int puntuacion) {
		this.puntuacion = puntuacion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public boolean isEstado() {
		return estado;
	}
	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return nivel+" "+nombre+" "+puntuacion+" "+estado; 
	}
}
