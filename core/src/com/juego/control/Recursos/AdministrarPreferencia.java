package com.juego.control.Recursos;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonWriter;
import com.badlogic.gdx.utils.JsonWriter.OutputType;

public class AdministrarPreferencia {
	
	public static void guardarNiveles(ArrayList<Nivel> nivels){
		
		Preferences preferences =Gdx.app.getPreferences("niveles");
		
		Json json=new Json();
		json.setOutputType(OutputType.json);
		String str =json.prettyPrint(nivels);
		preferences.putString("niveles",str);
		preferences.flush();
		
	}
	public static ArrayList<Nivel> leerListaNiveles(){
		ArrayList<Nivel>nivels=null;
		
		
		Preferences preferences=Gdx.app.getPreferences("niveles");
		String nivel= preferences.getString("niveles");
		Json json=new Json();
		nivels=json.fromJson(ArrayList.class,nivel);
		
		return nivels;
		
		
	}
	public static void guardarNivel(Nivel n){
		
		Preferences preferences =Gdx.app.getPreferences("nivel");
		
		Json json=new Json();
		json.setOutputType(OutputType.json);
		String str =json.prettyPrint(n);
		preferences.putString("nivel",str);
		preferences.flush();
		
	}
	public static Nivel leerNivel(){
		Nivel n = null;
		Preferences preferences=Gdx.app.getPreferences("nivel");
		String nivel= preferences.getString("nivel");
		Json json=new Json();
		n=json.fromJson(Nivel.class,nivel);
		
		
		
		return n;
		
	}
}
