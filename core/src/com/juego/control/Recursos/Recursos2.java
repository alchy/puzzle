package com.juego.control.Recursos;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.juego.Presentacion.Utilidad.AnimationCreator;


public class Recursos2 {
	private final static String FILE_IMAGE_ATLAS = "data/imageatlas.txt";
	private final static String FILE_IMAGE_ATLAS_2 = "data/imageatlas_2.txt";
	private final static String FILE_UI_SKIN = "skin/uiskin.json";
	private final static String FILE_NIVEL_ACTUAL="niveles/";
	private final static String FILE_NIVEL_1="niveles/nivel1.jpg";
	private final static String FILE_AYUDA="data/help.png";
	private final static String FILE_ICONO ="data/icono.png";
	private final static String FILE_PUZZLE ="menu/jigwawMenu.png";
	private final static String FILE_FIFTEEN="menU/jigwawMenu0.png";
	public static TextureAtlas imageAtlas;
	public static TextureAtlas imageAtlas_2;
	public static Skin skin;

	//
	public static TextureRegion img_bg_1_;
	public static TextureRegion img_btn_home;
	public static TextureRegion img_btn_home_pressed;
	public static TextureRegion img_obj_mtxlogo;
	public static TextureRegion img_obj_techo_disc;
	public static TextureRegion img_obj_trns_black;
	public static TextureRegion img_obj_trns_black_2_;
	public static TextureRegion img_obj_heart;
	public static TextureRegion img_obj_actor_1_;
	public static TextureRegion img_obj_actor_2_;
	public static TextureRegion img_obj_actor_3_;
	public static TextureRegion img_obj_fire;
	public static TextureRegion img_btn_genereal;
	public static TextureRegion img_btn_genereal_pressed;
	public static TextureRegion img_obj_arrow;
	public static TextureRegion img_obj_cloud_1_;
	public static TextureRegion img_obj_heart_1_;
	public static TextureRegion img_obj_heart_2_;
	public static TextureRegion img_obj_heart_shine_1_;
	public static TextureRegion img_obj_metal_ball;
	public static TextureRegion img_obj_snowflake_1_;
	public static TextureRegion img_obj_tech_inner_1_;
	public static TextureRegion img_obj_tech_outer_1_;
	public static TextureRegion img_obj_ayuda;
	public static TextureRegion img_obj_nivel1;
	public static TextureRegion img_obj_nivel_Actual;
	public static TextureRegion img_obj_icono;
	public static TextureRegion img_obj_puzzle_boton;
	public static TextureRegion img_obj_puzzle_fifteen;
	//
	public static Animation anim_fire;
	public static Animation anim_ignite;
	public static Animation anim_bat_fly;

	//
	public static BitmapFont font2_1;
	public static BitmapFont font2_2;

	// TEST OBJECT
	public static TextureRegion imgTest;
	public static BitmapFont font3_1;

	public static Texture loadTexture(String file) {
		return new Texture(Gdx.files.internal(file));
	}

	public static TextureAtlas getAtlas() {
		if (imageAtlas == null) {
			imageAtlas = new TextureAtlas(Gdx.files.internal(FILE_IMAGE_ATLAS));
		}
		return imageAtlas;
	}

	public static TextureAtlas getAtlas_2() {
		if (imageAtlas_2 == null) {
			imageAtlas_2 = new TextureAtlas(
					Gdx.files.internal(FILE_IMAGE_ATLAS_2));
		}
		return imageAtlas_2;
	}

	public static Skin getSkin() {
		if (skin == null) {
			FileHandle skinFile = Gdx.files.internal(FILE_UI_SKIN);
			skin = new Skin(skinFile);
		}
		return skin;
	}

	public static void loadAll() {
		relaseResources();
		loadImages();
		loadButtons();
		loadFonts();
		loadAnimations();
		loadSoundsAndMusics();
	}

	private static void relaseResources() {
		skin = null;
		imageAtlas = null;
		imageAtlas_2 = null;
	}

	public static void loadImages() {
		img_bg_1_ = getAtlas().findRegion("img_bg_1_");
		img_obj_mtxlogo = getAtlas().findRegion("img_obj_mtxlogo");
		img_obj_techo_disc = getAtlas().findRegion("img_obj_techo_disc");
		img_obj_trns_black = getAtlas().findRegion("img_obj_trns_black");
		img_obj_trns_black_2_ = getAtlas().findRegion("img_obj_trns_black_2_");
		img_obj_heart = getAtlas().findRegion("img_obj_heart");
		img_obj_actor_1_ = getAtlas().findRegion("img_obj_actor_1_");
		img_obj_actor_2_ = getAtlas().findRegion("img_obj_actor_2_");
		img_obj_actor_3_ = getAtlas().findRegion("img_obj_actor_3_");
		img_btn_genereal = getAtlas().findRegion("img_btn_genereal");
		img_btn_genereal_pressed = getAtlas().findRegion(
				"img_btn_genereal_pressed");
		img_obj_fire = getAtlas().findRegion("anim_fire_01");
		//
		img_obj_arrow = getAtlas_2().findRegion("img_obj_arrow");
		img_obj_cloud_1_ = getAtlas_2().findRegion("img_obj_cloud_1_");
		img_obj_heart_1_ = getAtlas_2().findRegion("img_obj_heart_1_");
		img_obj_heart_2_ = getAtlas_2().findRegion("img_obj_heart_2_");
		img_obj_heart_shine_1_ = getAtlas_2().findRegion(
				"img_obj_heart_shine_1_");
		img_obj_metal_ball = getAtlas_2().findRegion("img_obj_metal_ball");
		img_obj_snowflake_1_ = getAtlas_2().findRegion("img_obj_snowflake_1_");
		img_obj_tech_inner_1_ = getAtlas_2()
				.findRegion("img_obj_tech_inner_1_");
		img_obj_tech_outer_1_ = getAtlas_2()
				.findRegion("img_obj_tech_outer_1_");
		img_obj_ayuda=new TextureRegion(new Texture(FILE_AYUDA));
		img_obj_nivel1 =new TextureRegion(new Texture(FILE_NIVEL_1));
		img_obj_icono= new TextureRegion(new Texture(FILE_ICONO));
		img_obj_puzzle_boton=new TextureRegion(new Texture(FILE_PUZZLE));
		img_obj_puzzle_fifteen=new TextureRegion(new Texture(FILE_FIFTEEN));
	}

	public static void loadButtons() {
		img_btn_home = getAtlas().findRegion("img_btn_home");
		img_btn_home_pressed = getAtlas().findRegion("img_btn_home_pressed");
	}

	public static void loadFonts() {
		//
		font2_1 = new BitmapFont(Gdx.files.internal("data/font2.fnt"),
				Gdx.files.internal("data/font2.png"), false);
		font2_2 = new BitmapFont(Gdx.files.internal("data/font2.fnt"),
				Gdx.files.internal("data/font2.png"), false);
		font3_1 = new BitmapFont(Gdx.files.internal("data/font3.fnt"),
				Gdx.files.internal("data/font3.png"), false);
	}

	public static void loadAnimations() {
		anim_fire = AnimationCreator.getAnimationFromMultiTextures(
				getAtlas_2(), "anim_fire_", 6, 0.06f);
		anim_ignite = AnimationCreator.getAnimationFromMultiTextures(
				getAtlas_2(), "anim_ignite_", 6, 0.06f);
		anim_bat_fly = AnimationCreator.getAnimationFromSingleTexture(
				getAtlas_2(), "anim_bat_fly", 6, 0.06f);

	}

	public static void loadSoundsAndMusics() {
	
	}
	public static TextureRegion cargarNivelActual(String nivel){
		TextureRegion region=new TextureRegion(new Texture(FILE_NIVEL_ACTUAL+nivel+".jpg"));
		return region;
		
	}
	
}
