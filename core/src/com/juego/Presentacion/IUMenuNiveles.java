package com.juego.Presentacion;

import java.util.ArrayList;
import java.util.Random;











import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Json;
import com.juego.Presentacion.Pantallas.AbstractScreen;
import com.juego.Presentacion.Pantallas.IScreen;
import com.juego.Presentacion.Utilidad.ButtonLevel;
import com.juego.Presentacion.Utilidad.MenuCreator;
import com.juego.Presentacion.Utilidad.TableModel;
import com.juego.control.Recursos.AdministrarPreferencia;
import com.juego.control.Recursos.Nivel;
import com.juego.control.Recursos.Recursos1;
import com.juego.control.Recursos.Recursos2;

public class IUMenuNiveles extends AbstractScreen implements IScreen{

	private TableModel tablaMenuNiveles;
	private ArrayList<Nivel> niveles;
	public IUMenuNiveles(Game game, String screenName) {
		super(game, screenName);
		// TODO Auto-generated constructor stub
		
		Preferences preferences=Gdx.app.getPreferences("niveles");
		String str=preferences.getString("niveles");
		Json json=new Json();
		niveles=json.fromJson(ArrayList.class,str);
		
		setUpInfoPanel();
		setUpMenu();
		setUpScreenElements();
		setBackButtonActive(true);

		
	}

	@Override
	public void setUpScreenElements() {
		// TODO Auto-generated method stub
		tablaMenuNiveles = MenuCreator.createTable(true, Recursos1.skin);
		tablaMenuNiveles.setPosition(-999, 0);
		tablaMenuNiveles.addAction(Actions.moveTo(0, 0, 0.7f));
		tablaMenuNiveles.top().left().pad(10, 10, 10, 10);
		
		
		getStage().addActor(tablaMenuNiveles);
		

		int numberOfLevels = 16;
		
		// Create buttons with a loop
		for (int i = 0; i < numberOfLevels; i++){
			
			Nivel nivel=niveles.get(i);
			
			
			final ButtonLevel levelButton = MenuCreator.createCustomLevelButton(Recursos1.btnLevel,Recursos1.btnLevelPressed);
			
			levelButton.setSize(80, 80);
		
			levelButton.setLevelNumber(i + 1, Recursos1.font2);
			 
			
			levelButton.setLevelStars(Recursos1.imgStarHolder, Recursos1.imgStar, 3, nivel.getPuntuacion());
			if (nivel.isEstado()){
				levelButton.setDisabled(true);
			
			
				levelButton.addListener(new ActorGestureListener() {
				@Override
					public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
						super.touchUp(event, x, y, pointer, button);
						
						int k=levelButton.getLevelNumber()-1;
						Nivel n=niveles.get(k);
						AdministrarPreferencia.guardarNivel(n);
						IUJuego juego=new IUJuego(getGame(),"IUJuego");
						getGame().setScreen(juego);
					}
				});
			}else{
				
				levelButton.setColor(.2f,.2f,.2f,1f);
				
			}

			if(i % 4 == 0){
				tablaMenuNiveles.row();
			}
			
			
			if (Gdx.app.getType().equals(ApplicationType.Android))
				tablaMenuNiveles.add(levelButton).size(150,150).pad(4, 4, 4, 4).expand();
			else if(Gdx.app.getType().equals(ApplicationType.Desktop))
				tablaMenuNiveles.add(levelButton).size(100,100).pad(5, 5, 5, 5).expand();
		}		

	}

	@Override
	public void setUpInfoPanel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setUpMenu() {
		// TODO Auto-generated method stub
		setBackgroundTexture(Recursos2.img_bg_1_);
		setBackButtonActive(true);
	}
	


	@Override
	public void keyBackPressed() {
		super.keyBackPressed();
		
		getGame().setScreen(new IUPrincipal(getGame(),"IUMenu Principal"));
	}


}
