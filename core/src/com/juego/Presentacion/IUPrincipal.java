package com.juego.Presentacion;

import java.util.ArrayList;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonWriter.OutputType;
import com.juego.Presentacion.Pantallas.AbstractScreen;
import com.juego.Presentacion.Pantallas.IScreen;
import com.juego.Presentacion.Utilidad.AppSettings;
import com.juego.Presentacion.Utilidad.ButtonGame;
import com.juego.Presentacion.Utilidad.ButtonToggle;
import com.juego.Presentacion.Utilidad.EmptyAbstractActor;
import com.juego.Presentacion.Utilidad.MenuCreator;
import com.juego.Presentacion.Utilidad.TableModel;
import com.juego.control.Recursos.AdministrarPreferencia;
import com.juego.control.Recursos.Nivel;
import com.juego.control.Recursos.Recursos2;


public class IUPrincipal extends AbstractScreen implements IScreen{
	EmptyAbstractActor testActor;

	public IUPrincipal(Game game, String screenName) {
		super(game, screenName);
		//
		setUpScreenElements();
		setUpInfoPanel();
		setUpActors();
		setUpMenu();
		
		
		Preferences preferences=Gdx.app.getPreferences("niveles");
		String str2=preferences.getString("niveles");
		
		if (str2.length()<=2){
			Json json=new Json();
			ArrayList<Nivel> niveles=new ArrayList<Nivel>();
			iniciarNiveles(niveles);
			json.setOutputType(OutputType.json);
			String str =json.prettyPrint(niveles);
			preferences.putString("niveles",str);
			preferences.flush();
			
			AdministrarPreferencia.guardarNivel(niveles.get(0));
		}
		preferences.flush();
		setBackButtonActive(true);
	}
	

	public void iniciarNiveles(ArrayList<Nivel>nivels){
		for (int i = 1; i <=16; i++) {
			if (i==1){
				Nivel nivel=new Nivel(i,0,"nivel"+i,true);
				nivels.add(nivel);
			}
			else {
				Nivel nivel=new Nivel(i,0,"nivel"+i,false);
				nivels.add(nivel);	
			}
			
		}
		
	}
	@Override
	public void setUpScreenElements() {
		setBackgroundTexture(Recursos2.img_bg_1_);
		setBackButtonActive(true);
	}

	@Override
	public void setUpInfoPanel() {
		ButtonToggle icono= MenuCreator.createCustomToggleButton(Recursos2.font3_1,Recursos2.img_obj_icono, Recursos2.img_obj_icono, true
				,150, 130, true);
		icono.setPosition(50, AppSettings.SCREEN_H-200);
		getStage().addActor(icono);
	
	}

	private void setUpActors() {

		
	}

	@Override
	public void setUpMenu() {
		
		TableModel tableMenu = new TableModel(null,
				AppSettings.WORLD_WIDTH / 3.0f, AppSettings.WORLD_HEIGHT);
		tableMenu.setPosition(AppSettings.WORLD_WIDTH + AppSettings.WORLD_WIDTH / 3.0f,
			0 - (30 * AppSettings.getWorldPositionYRatio()));
	
		
		tableMenu.addAction(Actions.moveTo(AppSettings.SCREEN_W-AppSettings.WORLD_WIDTH / 3.0f-30,
				tableMenu.getY(), 0.5f));

	
		ButtonGame btnIniciar = MenuCreator.createCustomGameButton(
				Recursos2.font3_1, Recursos2.img_btn_genereal,
				Recursos2.img_btn_genereal_pressed, 250, 80, true);
		btnIniciar.setText("Iniciar", true);
		btnIniciar.setTextPosXY(30, 70);
		btnIniciar.addListener(new ActorGestureListener() {
			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				super.touchUp(event, x, y, pointer, button);
				getGame().setScreen(new IUMenuNiveles(getGame(),"IU Menu Niveles"));
			
				
				
				
			}
		});


		ButtonGame btnAcercaDe = MenuCreator.createCustomGameButton(Recursos2.font3_1,
				Recursos2.img_btn_genereal, Recursos2.img_btn_genereal_pressed, 280,
				80, true);
		btnAcercaDe.setText("App Inf ", true);
		btnAcercaDe.setTextPosXY(30, 70);
		btnAcercaDe.addListener(new ActorGestureListener() {
			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				super.touchUp(event, x, y, pointer, button);
				//
				getGame().setScreen(new IUAcercaDe(getGame(), "IUAcercade"));
			}
		});


		ButtonGame btnSalir = MenuCreator.createCustomGameButton(
				Recursos2.font3_1, Recursos2.img_btn_genereal,
				Recursos2.img_btn_genereal_pressed, 250, 80, true);
		btnSalir.setText("Salir", true);
		btnSalir.setTextPosXY(30, 70);
		btnSalir.addListener(new ActorGestureListener() {
			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				super.touchUp(event, x, y, pointer, button);
				//
				if (getSecondsTime() > 10f) {
					Gdx.app.exit();
				}
			}
		});

		

	
		float dipRatioWidth = 265 * AppSettings.getWorldSizeRatio();
		float dipRatioHeight = 80 * AppSettings.getWorldSizeRatio();
		float padding = 5.0f * AppSettings.getWorldSizeRatio();


		tableMenu.add(btnIniciar).size(dipRatioWidth, dipRatioHeight)
				.pad(padding);
		tableMenu.row();
		tableMenu.add(btnAcercaDe).size(dipRatioWidth, dipRatioHeight).pad(padding);
		tableMenu.row();
		tableMenu.add(btnSalir).size(dipRatioWidth, dipRatioHeight)
				.pad(padding);
		tableMenu.row();
		getStage().addActor(tableMenu);
		
		ButtonGame buttonPuzzle=MenuCreator.createCustomGameButton(Recursos2.font3_1, Recursos2.img_obj_puzzle_boton, Recursos2.img_obj_puzzle_boton );
		buttonPuzzle.setSize(100,100);
		buttonPuzzle.setPosition(80, 100);
		ButtonGame buttonFifteen=MenuCreator.createCustomGameButton(Recursos2.font3_1,Recursos2.img_obj_puzzle_fifteen,Recursos2.img_obj_puzzle_fifteen);
		buttonFifteen.setSize(100, 100);
		buttonFifteen.setPosition(200, 100);
		getStage().addActor(buttonFifteen);
		getStage().addActor(buttonPuzzle);
		
	}

	@Override
	public void keyBackPressed() {
		super.keyBackPressed();
		
		if (getSecondsTime() > 10f) {
			Gdx.app.exit();
		}
	}
}
