package com.juego.Presentacion.Principal;

import com.badlogic.gdx.Game;
import com.juego.Presentacion.IUPrincipal;
import com.juego.Presentacion.Utilidad.AppSettings;
import com.juego.control.Recursos.Recursos1;
import com.juego.control.Recursos.Recursos2;

public class GamePrincipal extends Game {

	@Override
	public void create() {
		// TODO Auto-generated method stub
		Recursos2.loadAll();
		Recursos1.loadAll();
		AppSettings.setUp();
		setScreen(new IUPrincipal(this, "IUPrincipal-Menu Principal"));
	}
	@Override
	public void render() {
		// TODO Auto-generated method stub
		super.render();
		
	}

}
