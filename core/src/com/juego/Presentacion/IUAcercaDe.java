package com.juego.Presentacion;



import java.util.Iterator;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.juego.Presentacion.Pantallas.AbstractScreen;
import com.juego.Presentacion.Pantallas.IScreen;
import com.juego.Presentacion.Utilidad.ButtonLevel;
import com.juego.Presentacion.Utilidad.Elemento;
import com.juego.Presentacion.Utilidad.MenuCreator;
import com.juego.control.Recursos.Recursos2;

import com.sun.corba.se.impl.interceptors.PICurrent;

public class IUAcercaDe extends AbstractScreen implements IScreen  {

	public IUAcercaDe(Game game, String screenName) {
		super(game, screenName);
		// TODO Auto-generated constructor stub
		setUpInfoPanel();
		setUpMenu();
		setUpScreenElements();
	}

	@Override
	public void setUpScreenElements() {
		// TODO Auto-generated method stub
		setUpTouchListeners();
	}

	@Override
	public void setUpInfoPanel() {
		// TODO Auto-generated method stub
		setBackgroundTexture(Recursos2.img_bg_1_);
	}

	@Override
	public void setUpMenu() {
		// TODO Auto-generated method stub
		Pixmap pixmap=new Pixmap(Gdx.files.local("molde/molde3x3.png"));
		Pixmap pixmap3=new Pixmap(Gdx.files.local("niveles/nivel1.jpg"));
		Pixmap pixmap2=new Pixmap(512,512,Format.RGBA8888);
		pixmap2.setColor(255);
		
		
		Array<Elemento>elemntos= obtenerElementos(pixmap,pixmap3);
		
		for (Elemento elemento : elemntos) {
			
		
				Pixmap p=elemento.getPixmapEmlemento();
				p.setColor(Color.GREEN);
				TextureRegion t=new TextureRegion(new Texture(p));
				TextureRegion t1=new TextureRegion(new Texture(elemento.getPixmapEmlemento()));
				ButtonLevel buttonLevel=MenuCreator.createCustomLevelButton(t,t1);
							
				//buttonLevel.setsi
				buttonLevel.setPosition(100, 100);
				
				getStage().addActor(buttonLevel);
			}
		
		
	
		
		
		
	}
	public Elemento buscarPixel(Array<Elemento> elementos, int pixel){
		for (Elemento elemento : elementos) {
			if (elemento.getPixel()==pixel){
				return elemento;
				
			}
		}
		return null;
	}
	public Array<Elemento> obtenerElementos(Pixmap pixmapMolde,Pixmap pixmapSrc){
		Array<Elemento>elementos=new Array<Elemento>();
		for (int i = 0; i < pixmapMolde.getHeight(); i++) {
			for (int j = 0; j < pixmapMolde.getWidth(); j++) {
				int pixel=pixmapMolde.getPixel(i, j);
				Elemento elemento=buscarPixel(elementos, pixel);
				int z=200;
					if (elemento!=null){
						///agregar Pixel
						Pixmap pixmapAux=elemento.getPixmapEmlemento();
						pixmapAux.drawPixel(i-elemento.getxInit()+z, j-elemento.getyInit()+z,pixmapSrc.getPixel(i, j));
						
					}else{
						//agregar color y pixel
						Elemento elemento2=new Elemento();
						elemento2.setPixel(pixel);
						elemento2.setxInit(i);
						elemento2.setyInit(j);
						Pixmap pixmapAux=new Pixmap(512,512, Format.RGBA8888);
						pixmapAux.setColor(Color.BLACK);
						pixmapAux.drawRectangle(0, 0,512,512);
						pixmapAux.drawPixel(i-elemento2.getxInit()+z, j-elemento2.getyInit()+z,pixmapSrc.getPixel(i, j));
						elemento2.setPixmapEmlemento(pixmapAux);
						elementos.add(elemento2);
					}
			}
		}
		
		
		return elementos;
	}
	
	private void setUpTouchListeners() {
		getStage().addListener(new ActorGestureListener() {
			// Touch Down an actor
			@Override
			public void touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				super.touchDown(event, x, y, pointer, button);
			}

			// Pan/Drag an actor
			@Override
			public void pan(InputEvent event, float x, float y, float deltaX,
					float deltaY) {
				super.pan(event, x, y, deltaX, deltaY);

				// #06.5 Test
				// If touch town actor is a TestActor, drag along the finger
				// ###########################################
				if (getTouchDownTarget() instanceof ButtonLevel) {
					
					getTouchDownTarget().setPosition(
							getTouchDownTarget().getX() + deltaX,
							getTouchDownTarget().getY() + deltaY);
				}

			}

			// Touch Up an actor
			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				super.touchUp(event, x, y, pointer, button);
			}
		});
	}

	

}
