package com.juego.Presentacion;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.juego.Presentacion.Pantallas.AbstractScreen;
import com.juego.Presentacion.Pantallas.IScreen;
import com.juego.Presentacion.Utilidad.AppSettings;
import com.juego.Presentacion.Utilidad.ButtonLevel;
import com.juego.Presentacion.Utilidad.ButtonToggle;
import com.juego.Presentacion.Utilidad.MenuCreator;
import com.juego.control.Recursos.AdministrarPreferencia;
import com.juego.control.Recursos.Nivel;
import com.juego.control.Recursos.Recursos2;

public class IUAyuda extends AbstractScreen implements IScreen {

	private Nivel nivel;
	private IUJuego juego;
	public IUAyuda(Game game, String screenName) {
		super(game, screenName);
		// TODO Auto-generated constructor stub
		nivel=AdministrarPreferencia.leerNivel();
		setUpInfoPanel();
		setUpScreenElements();
		setUpMenu();
		setBackButtonActive(true);
		
		
	}

	@Override
	public void setUpScreenElements() {
		// TODO Auto-generated method stub
		setBackgroundTexture(Recursos2.img_bg_1_);
		ButtonToggle buttonToggle=MenuCreator.createCustomToggleButton(Recursos2.font3_1,Recursos2.img_btn_home, 
				Recursos2.img_btn_home_pressed, true);
		buttonToggle.setPosition(AppSettings.SCREEN_W-buttonToggle.getWidth(), AppSettings.SCREEN_H-buttonToggle.getHeight());
		buttonToggle.addListener(new ActorGestureListener(){

			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				// TODO Auto-generated method stub
				super.touchUp(event, x, y, pointer, button);
				IUJuego juego=new IUJuego(getGame(), "IU juego ");
				getGame().setScreen(juego);
			}
			
		});
		getStage().addActor(buttonToggle);
	}

	@Override
	public void setUpInfoPanel() {
		// TODO Auto-generated method stub
		setBackButtonActive(true);
	}

	@Override
	public void setUpMenu() {
		// TODO Auto-generated method stub

		ButtonLevel buttonLevel=MenuCreator.crearBotonAyuda(nivel.getNombre());
		
		buttonLevel.setSize(420,420);
		
		buttonLevel.setPosition((AppSettings.SCREEN_W/2)-buttonLevel.getWidth()/2, AppSettings.SCREEN_H/2-buttonLevel.getHeight()/2);
		buttonLevel.addListener(new ActorGestureListener(){
			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				// TODO Auto-generated method stub
				super.touchUp(event, x, y, pointer, button);
				IUJuego juego=new IUJuego(getGame(), "IU juego ");
				getGame().setScreen(juego);
			}
		});

		
		
		getStage().addActor(buttonLevel);
	}
	public void setIUJuego(IUJuego juego){
		this.juego=juego;
		
	}
	public IUJuego getIUJuego(){
		
		return juego;
	}
	public void setNivel( Nivel nivel) {
		// TODO Auto-generated method stub
		this.nivel=nivel;
		
	}
	 @Override
	public void keyBackPressed() {
		// TODO Auto-generated method stub
		super.keyBackPressed();
		
		getGame().setScreen(new IUJuego(getGame(), "IU Juego"));
	}

}
