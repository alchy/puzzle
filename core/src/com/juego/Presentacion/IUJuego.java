package com.juego.Presentacion;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Array;
import com.juego.Presentacion.Pantallas.AbstractScreen;
import com.juego.Presentacion.Pantallas.IScreen;
import com.juego.Presentacion.Utilidad.AppSettings;
import com.juego.Presentacion.Utilidad.ButtonLevel;
import com.juego.Presentacion.Utilidad.ButtonToggle;
import com.juego.Presentacion.Utilidad.InputIntent;
import com.juego.Presentacion.Utilidad.MenuCreator;
import com.juego.Presentacion.Utilidad.InputIntent.DirectionIntent;
import com.juego.control.Recursos.AdministrarPreferencia;
import com.juego.control.Recursos.Nivel;
import com.juego.control.Recursos.Recursos2;
import com.juego.control.Rompecabeza.Movible;
import com.juego.control.Rompecabeza.MovimientoEzquinas;
import com.juego.control.Rompecabeza.MovimientoGeneral;
import com.juego.control.Rompecabeza.MovimientoLaterales;

public class IUJuego extends AbstractScreen implements IScreen{

	
	private com.juego.Presentacion.Utilidad.InputIntent inputIntent;
	private FachadaMesa fachadaMesa;
	private Movible movimientoEzquinas;
	
	
	
	
	
	
	
	static boolean condicion=true;
	private Nivel nivel;
	private int dragAmount;
	public IUJuego(Game game, String screenName) {
		super(game, screenName);
		// TODO Auto-generated constructor stub
		
		
		nivel=AdministrarPreferencia.leerNivel();
		
		fachadaMesa=new FachadaMesa();
		
		Movible movimientoLaterales;
		Movible movimientoGeneral;
		movimientoEzquinas=new MovimientoEzquinas(fachadaMesa.getMesa());
		movimientoLaterales=new MovimientoLaterales(fachadaMesa.getMesa());
		movimientoGeneral=new MovimientoGeneral(fachadaMesa.getMesa());
		
		movimientoEzquinas.setNextMovible(movimientoLaterales);
		movimientoLaterales.setNextMovible(movimientoGeneral);
		
		
		
		setUpInfoPanel();
		setUpScreenElements();
		setUpMenu();
		setUpTouchListeners();
		setBackButtonActive(true);
		pintar();
	}

	
	@Override
	public void setUpScreenElements() {
		// TODO Auto-generated method stub
		
	
	}
	
	
	public void elimimarFichas(){
		Array<Actor> actores= getStage().getActors();
		for (Actor actor : actores) {
			if (actor instanceof ButtonLevel){
				actor.remove();
			}
		}
		
	}	
	@Override
	public void setUpInfoPanel() {
		// TODO Auto-generated method stub
		setBackgroundTexture(Recursos2.img_bg_1_);
		setBackButtonActive(true);
		
	}

	@Override
	public void setUpMenu() {
		// TODO Auto-generated method stub
		ButtonToggle buttonToggle=MenuCreator.createCustomToggleButton(Recursos2.font3_1,Recursos2.img_btn_home, 
				Recursos2.img_btn_home_pressed, true);
		
		buttonToggle.setPosition(AppSettings.SCREEN_W-buttonToggle.getWidth(), AppSettings.SCREEN_H-buttonToggle.getHeight());
		buttonToggle.addListener(new ActorGestureListener(){

			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				// TODO Auto-generated method stub
				super.touchUp(event, x, y, pointer, button);
				getGame().setScreen(new IUMenuNiveles(getGame(), "IUMenu Niveles"));
			}
			
		});
		getStage().addActor(buttonToggle);
		ButtonToggle bottonAyuda=MenuCreator.createCustomToggleButton(Recursos2.font3_1,Recursos2.img_obj_ayuda , Recursos2.img_obj_ayuda ,true);
		bottonAyuda.setSize(80, 80);
		bottonAyuda.setPosition(10, AppSettings.SCREEN_H-bottonAyuda.getHeight()-20);
		bottonAyuda.addListener(new ActorGestureListener(){
			
			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				// TODO Auto-generated method stub
				super.touchUp(event, x, y, pointer, button);
				
				IUAyuda ayuda=new IUAyuda(getGame(), "IU Ayuda");
				
				getGame().setScreen(ayuda);
			}
		});
		getStage().addActor(bottonAyuda);
	}
	

	@Override
	public void keyBackPressed() {
		super.keyBackPressed();

		getGame().setScreen(new IUMenuNiveles(getGame(), "IUMenu Niveles"));
				
	}

	private void setUpTouchListeners() {
	
		
		dragAmount=80;
		inputIntent=new InputIntent();
		inputIntent.setTouchDragIntervalRange(dragAmount);

		getStage().addListener(new ActorGestureListener() {
			// Touch Down an actor
			@Override
			public void touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				super.touchDown(event, x, y, pointer, button);

				// #05.5 Test
				// Set touch down initials for input intent
				// ########################################
				inputIntent.setTouchInitials(x, y);
				condicion=true;

			}

	
			@Override
			public void pan(InputEvent event, float x, float y, float deltaX,
					float deltaY) {
				super.pan(event, x, y, deltaX, deltaY);

				inputIntent.setTouchCurrents(x, y);
				inputIntent.setTouchDifferences();

			
				if (inputIntent.getDirectionIntent() == DirectionIntent.TOUCH_D_RIGHT) {
					inputIntent.setTouchDifferences();
					if (inputIntent.isTouchDragInterval()) {
						if (inputIntent.isTouchDragInterval()) {
						if (condicion)
							movimientoEzquinas.mover(fachadaMesa.getMesa().getElementAt('r'));
							pintar();
							condicion=false;
						//inputIntent.reset();
						}
					}
				}
				if (inputIntent.getDirectionIntent()==DirectionIntent.TOUCH_D_UP) {
					
					if (inputIntent.isTouchDragInterval()) {
						if (condicion)
							movimientoEzquinas.mover(fachadaMesa.getMesa().getElementAt('u'));
							pintar();
							condicion=false;
						//inputIntent.reset();
						}				}
				if (inputIntent.getDirectionIntent()==DirectionIntent.TOUCH_D_DOWN) {
					if (inputIntent.isTouchDragInterval()) {
						if (condicion)
							movimientoEzquinas.mover(fachadaMesa.getMesa().getElementAt('d'));
							pintar();
							condicion=false;
						//inputIntent.reset();
						}				}

				
				if (inputIntent.getDirectionIntent() == DirectionIntent.TOUCH_D_LEFT) {
					

					if (inputIntent.isTouchDragInterval()) {
						if (inputIntent.isTouchDragInterval()) {
							if (condicion){
								movimientoEzquinas.mover(fachadaMesa.getMesa().getElementAt('l'));
								pintar();
								condicion=false;
							//inputIntent.reset();
							}
							
					}
				}

			}
	}

			// Touch Up an actor
			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				super.touchUp(event, x, y, pointer, button);

				inputIntent.reset();
				condicion=true;
				//
				
			}
		});
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		super.render(delta);
		
		if(fachadaMesa.getMesa().juegoGanado())
		{
			pintar();
			
			nivel.setPuntuacion(3);
			ArrayList<Nivel> niveles=AdministrarPreferencia.leerListaNiveles();
			niveles.get(nivel.getNivel()-1).setPuntuacion(3);
			if(nivel.getNivel()-1<=15){
				niveles.get(nivel.getNivel()).setEstado(true);
				AdministrarPreferencia.guardarNiveles(niveles);
				getGame().setScreen(new IUMenuNiveles(getGame(), "IU Menu Niveles"));
			}
			else{
				
			}
			
			


			
			
		}
	}


	private void pintar() {
		// TODO Auto-generated method stub
		getStage().clear();
		setUpInfoPanel();
		setUpScreenElements();
		setUpMenu();
		setUpTouchListeners();
		int ancho=100;
		int alto=100;
		float x=AppSettings.SCREEN_W/2.f-160;;
		float y=AppSettings.SCREEN_H/2.f;
		int z=160;
		if(Gdx.app.getType()==ApplicationType.Desktop){
		 ancho=100;
		 alto=100;
		 z=160;
		 x=AppSettings.SCREEN_W/2.f-z;
		 y=AppSettings.SCREEN_H/2.f;
		}
		else if (Gdx.app.getType()==ApplicationType.Android) {
			ancho=150;
			 alto=150;	
			 z=200;
			 x=AppSettings.SCREEN_W/2.f-z;
			 y=AppSettings.SCREEN_H/2.f+10;
		}
		 
		
		ButtonLevel fichas[][]=fachadaMesa.getRepresentable();
		for (int i = 0; i < fichas.length; i++) {
			for (int j = 0; j < fichas.length; j++) {
				ButtonLevel ficha=fichas[i][j];
				ficha.setSize(ancho, alto);
				ficha.setPosition(x, y);
				getStage().addActor(ficha);
				x=x+ancho;
			}
			x=AppSettings.SCREEN_W/2.f-z;
			y=y-alto;
		}
		
	}
	
	
	
}
