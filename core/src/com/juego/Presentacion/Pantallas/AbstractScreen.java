

package com.juego.Presentacion.Pantallas;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.juego.Presentacion.Utilidad.AppSettings;
import com.juego.Presentacion.Utilidad.MtxLogger;


public abstract class AbstractScreen implements Screen {
	//
	protected static final String logTag = "com.juego.presentacion";
	public static boolean logActivo = true;


	private Game juego;


	private String nombreScreen = "Desconocido";
	private final Stage stage;

	
	private float inicioHora = System.nanoTime();
	public static float SEGUNDOS = 0;

	
	private float stateTime = 0;


	private boolean isBackButtonActivo = false;


	public AbstractScreen(Game game, String screenName) {
		super();
		this.juego = game;
		if (screenName.equals("")) {
			this.nombreScreen = "Desconocido";
		} else {
			this.nombreScreen = screenName;
		}
		
		
		ScalingViewport scalingViewport = new ScalingViewport(Scaling.stretchX,AppSettings.WORLD_WIDTH,AppSettings.WORLD_HEIGHT);
		//stage = new Stage(scalingViewport);
		stage= new Stage();
		stage.getCamera().position.set(AppSettings.SCREEN_W / 2,
				AppSettings.SCREEN_H / 2, 0);

		// Receive inputs from stage
		Gdx.input.setInputProcessor(stage);

		// INFO LOG
		MtxLogger.log(logActivo, true, logTag, "SCREEN CONSTRUCTED: "
				+ getScreenName());
		
	}

	@Override
	public void render(float delta) {
		
		if (System.nanoTime() - inicioHora >= 1000000000) {
			SEGUNDOS++;
			inicioHora = System.nanoTime();
		}

	
		stateTime += delta;

		
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		
		stage.act(delta);

		
		stage.draw();

		
		if (isBackButtonActivo) {
			if (Gdx.input.isKeyPressed(Keys.BACK)) {
				keyBackPressed();
			}
		}
	}


	public void setBackgroundTexture(TextureRegion textureBackground) {
		Drawable tBg = new TextureRegionDrawable(textureBackground);
		Image imgbg = new Image(tBg, Scaling.stretch);
		imgbg.setFillParent(true);
		System.out.println("stage width: "+stage.getWidth()+" Height :"+stage.getHeight());
		System.out.println("AppSetting Width:" +AppSettings.SCREEN_W+" Height: "+AppSettings.SCREEN_H);
		//imgbg.setPosition(-stage.getWidth()/2,-stage.getHeight()/2 );
		imgbg.setPosition(0, 0);
		stage.addActor(imgbg);
		MtxLogger.log(logActivo, true, logTag, "SCREEN BG IMAGE SET: "
				+ getScreenName());
	}

	
	public void setBackButtonActive(boolean isBackButtonActive) {
		Gdx.input.setCatchBackKey(true);
		this.isBackButtonActivo = isBackButtonActive;
		//
		MtxLogger.log(logActivo, true, logTag, "SCREEN BACK BUTTON SET: "
				+ getScreenName());
	}

	
	public void keyBackPressed() {
	}

	
	public Game getGame() {
		return juego;
	}

	
	public void setGame(Game game) {
		this.juego = game;
	}

	
	public String getScreenName() {
		return nombreScreen;
	}

	
	public void setScreenName(String screenName) {
		this.nombreScreen = screenName;
	}

	
	public float getStartTime() {
		return inicioHora;
	}

	
	public float getSecondsTime() {
		return SEGUNDOS;
	}

	
	public void setSecondsTime(float secondsTime) {
		SEGUNDOS = secondsTime;
	}

	
	public float getStateTime() {
		return stateTime;
	}

	
	public void setStateTime(float stateTime) {
		this.stateTime = stateTime;
	}

	public boolean isBackButtonActive() {
		return isBackButtonActivo;
	}

	
	public Stage getStage() {
		return stage;
	}

	
	public String getScreenTime() {
		int seconds = (int) (SEGUNDOS % 60);
		int minutes = (int) ((SEGUNDOS / 60) % 60);
		int hours = (int) ((SEGUNDOS / 3600) % 24);
		String secondsStr = (seconds < 10 ? "0" : "") + seconds;
		String minutesStr = (minutes < 10 ? "0" : "") + minutes;
		String hoursStr = (hours < 10 ? "0" : "") + hours;
		return new String(hoursStr + ":" + minutesStr + ":" + secondsStr);
	}

	@Override
	public void resize(int width, int height) {
		MtxLogger.log(logActivo, true, logTag, "SCREEN RESIZE: "
				+ getScreenName());
	}

	@Override
	public void show() {
		MtxLogger.log(logActivo, true, logTag, "SCREEN SHOW: "
				+ getScreenName());
	}

	@Override
	public void hide() {
		MtxLogger.log(logActivo, true, logTag, "SCREEN HIDE: "
				+ getScreenName());
	}

	@Override
	public void pause() {
		MtxLogger.log(logActivo, true, logTag, "SCREEN PAUSE: "
				+ getScreenName());
	}

	@Override
	public void resume() {
		MtxLogger.log(logActivo, true, logTag, "SCREEN RESUME: "
				+ getScreenName());
	}

	@Override
	public void dispose() {
		stage.dispose();

		// Add items here for log
		String strDisposedItems = "Stage, ";
		MtxLogger.log(logActivo, true, logTag, "SCREEN DISPOSING: "
				+ getScreenName() + " DISPOSED: " + strDisposedItems);
	}
}
