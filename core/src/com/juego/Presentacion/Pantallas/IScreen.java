
package com.juego.Presentacion.Pantallas;

public interface IScreen {

	public void setUpScreenElements();

	public void setUpInfoPanel();


	public void setUpMenu();
}
