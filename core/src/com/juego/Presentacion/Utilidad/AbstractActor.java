/*******************************************************************************
 * Copyright 2012-Present, MoribitoTech
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.juego.Presentacion.Utilidad;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.MoveByAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;


public abstract class AbstractActor extends Actor {
	//
	protected final String logTag = "actorAbstracto";
	public static boolean logActive = true;


	private TextureRegion textureRegion;
	private boolean isTextureRegionActive = false;


	private Animation animation;
	private Animation animationMomentary;
	private boolean isAnimationActive = false;
	private boolean isAnimationMomentaryActive = false;
	private boolean isAnimationMomentaryFinished = true;
	private boolean isAnimationLooping = false;
	private boolean killAllAnimations = false;


	private float stateTime = 0;

	
	private ParticleEffect particleEffect;
	private float particlePosX = 0.0f;
	private float particlePosY = 0.0f;
	private boolean isParticleEffectActive;


	private float startTime = System.nanoTime();
	private float secondsTime = 0;

	public AbstractActor(TextureRegion textureRegion,
			boolean isTextureRegionActive, float posX, float posY, float orgnX,
			float orgnY, float width, float height) {
		super();
		this.textureRegion = textureRegion;
		this.isTextureRegionActive = isTextureRegionActive;
		setBounds(posX, posY, width, height);
		setPosition(posX, posY);
		setSize(width, height);
		setOrigin(orgnX, orgnY);
	}

	public AbstractActor(TextureRegion textureRegion,
			boolean isTextureRegionActive, float posX, float posY, float width,
			float height) {
		super();
		this.textureRegion = textureRegion;
		this.isTextureRegionActive = isTextureRegionActive;
		setBounds(posX, posY, width, height);
		setPosition(posX, posY);
		setSize(width, height);
	}

	public AbstractActor(float posX, float posY, float width, float height) {
		super();
		setBounds(posX, posY, width, height);
		setPosition(posX, posY);
		setSize(width, height);
	}

	public AbstractActor(float width, float height, boolean DIPActive) {
		super();
		if (DIPActive) {
			float ratioSize = AppSettings.getWorldSizeRatio();
			setSize(width * ratioSize, height * ratioSize);
		} else {
			setSize(width, height);
		}
	}

	public AbstractActor() {
		super();
	}

	@Override
	public void act(float delta) {
		super.act(delta);
		stateTime += delta;

	
		if (System.nanoTime() - startTime >= 1000000000) {
			secondsTime++;
			startTime = System.nanoTime();
		}
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		// For fade out/in effect
		batch.setColor(this.getColor());

		
		if (isTextureRegionActive && textureRegion != null) {
			
			batch.draw(textureRegion, getX(), getY(), getOriginX(),
					getOriginY(), getWidth(), getHeight(), getScaleX(),
					getScaleY(), getRotation());
		}

		
		if (isAnimationActive && animation != null) {
			
			TextureRegion keyFrame = animation.getKeyFrame(stateTime,
					isAnimationLooping);

		
			batch.draw(keyFrame, getX(), getY(), getOriginX(), getOriginY(),
					getWidth(), getHeight(), getScaleX(), getScaleY(),
					getRotation());

			if (animation.isAnimationFinished(stateTime)) {
				if (killAllAnimations) {
					isAnimationActive = false;
				}
			}
		}

		
		if (isAnimationMomentaryActive && animationMomentary != null) {
			if (animationMomentary.isAnimationFinished(stateTime)) {
				if (!killAllAnimations) {
					isAnimationActive = true;
					isAnimationMomentaryActive = false;
					isAnimationMomentaryFinished = true;
					startTime = 0;
				} else {
					isAnimationActive = false;
					isAnimationMomentaryActive = false;
					isAnimationMomentaryFinished = true;
					startTime = 0;
				}
			}

			if (isAnimationMomentaryActive) {
				// Get frame by frame and animate
				TextureRegion keyFrame = animationMomentary.getKeyFrame(
						stateTime, false);

				// Draw it due to actors' settings
				batch.draw(keyFrame, getX(), getY(), getOriginX(),
						getOriginY(), getWidth(), getHeight(), getScaleX(),
						getScaleY(), getRotation());
			}
		}

		
		if (isParticleEffectActive) {
			particleEffect.draw(batch, Gdx.graphics.getDeltaTime());
			particleEffect.setPosition(getX() + particlePosX, getY()
					+ particlePosY);
		}
	}

	public void addInputListeners() {
		addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				Gdx.app.log("InputLog", "TOUCH DOWN");
				return true;
			}

			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				Gdx.app.log("InputLog", "TOUCH UP");
			}
		});
	}

	public void actionMoveTo(float x, float y, float duration) {

		MoveToAction action = new MoveToAction();
		action.setPosition(x, y);
		if (duration > 0.0f) {
			action.setDuration(duration);
		}
		addAction(action);
	}

	public void actionMoveBy(float x, float y, float duration) {
		
		MoveByAction action = new MoveByAction();
		action.setAmount(x, y);
		if (duration > 0.0f) {
			action.setDuration(duration);
		}
		addAction(action);
	}


	public void translateWithoutAcc(float speedX, float speedY, float delta) {
		setPosition(getX() + (speedX * delta), getY() + (speedY * delta));
	}

	public TextureRegion getTextureRegion() {
		return textureRegion;
	}

	
	public void setTextureRegion(TextureRegion textureRegion,
			boolean isTextureRegionActive) {
		this.textureRegion = textureRegion;
		this.isTextureRegionActive = isTextureRegionActive;
	}

	public Animation getAnimation() {
		return animation;
	}


	public void setAnimation(Animation animation, boolean isAnimationActive,
			boolean isAnimationLooping) {
		this.animation = animation;
		this.isAnimationActive = isAnimationActive;
		this.isAnimationLooping = isAnimationLooping;
		//
	}

	public Animation getAnimationMomentary() {
		return animationMomentary;
	}

	
	public void setAnimationMomentary(Animation animationMomentary,
			boolean isAnimationMomentaryActive,
			Animation animationAfterMomentary,
			boolean isAnimationMomentaryWaitingToBeCompleted,
			boolean killAllAnimations) {
		this.killAllAnimations = killAllAnimations;

		if (animationAfterMomentary != null) {
			animation = animationAfterMomentary;
		}

		if (!isAnimationMomentaryWaitingToBeCompleted) {
			this.animationMomentary = animationMomentary;
			//this.animationMomentary.setPlayMode(Animation.NORMAL);
			this.isAnimationMomentaryActive = isAnimationMomentaryActive;
			if (isAnimationMomentaryActive) {
				stateTime = 0;
				isAnimationActive = false;
			}

		} else {
			if (isAnimationMomentaryFinished) {
				this.animationMomentary = animationMomentary;
				//this.animationMomentary.setPlayMode(Animation.NORMAL);
				this.isAnimationMomentaryActive = isAnimationMomentaryActive;
				//
				if (isAnimationMomentaryActive) {
					stateTime = 0;
					isAnimationActive = false;
				}

				isAnimationMomentaryFinished = false;
			}
		}
	}

	public boolean isAnimationActive() {
		return isAnimationActive;
	}

	
	public void setAnimationActive(boolean isAnimationActive) {
		this.isAnimationActive = isAnimationActive;
	}

	public boolean isAnimationLooping() {
		return isAnimationLooping;
	}


	public void setAnimationLooping(boolean isAnimationLooping) {
		this.isAnimationLooping = isAnimationLooping;
	}

	/**
	 * Returns the state time for this actor, it can be used in animations
	 * 
	 * @return state time (delta added)
	 * 
	 * */
	public float getStateTime() {
		return stateTime;
	}

	/**
	 * Set state time
	 */
	public void setStateTime(float stateTime) {
		this.stateTime = stateTime;
	}

	/**
	 * Get if killAllAnimation active
	 * */
	public boolean isKillAllAnimations() {
		return killAllAnimations;
	}

	/**
	 * Set killAllAnimations. If is true, after animations completed it wont be
	 * visible anymore
	 * */
	public void setKillAllAnimations(boolean killAllAnimations) {
		this.killAllAnimations = killAllAnimations;
	}

	/**
	 * Get seconds from the moment this actor created
	 * */
	public float getSecondsTime() {
		return secondsTime;
	}

	/**
	 * Set seconds, it can be used the reset seconds for this actor
	 * */
	public void setSecondsTime(float secondsTime) {
		this.secondsTime = secondsTime;
	}

	/**
	 * Get particle for this actor
	 * */
	public ParticleEffect getParticleEffect() {
		return particleEffect;
	}

	/**
	 * Set particle for this actor, centerPosition is used to center the
	 * particle on this actor sizes
	 * */
	public void setParticleEffect(ParticleEffect particleEffect,
			boolean isParticleEffectActive, boolean isStart,
			boolean centerPosition) {
		this.particleEffect = particleEffect;
		this.isParticleEffectActive = isParticleEffectActive;
		if (!centerPosition) {
			this.particleEffect.setPosition(getX(), getY());
		} else {
			particlePosX = getWidth() / 2.0f;
			particlePosY = getHeight() / 2.0f;
			this.particleEffect.setPosition(getX() + particlePosX, getY()
					+ particlePosY);
		}

		if (isStart) {
			this.particleEffect.start();
		}
	}

	/**
	 * Set particle position
	 * */
	public void setParticlePositionXY(float x, float y) {
		particlePosX = x;
		particlePosY = y;
	}

	/**
	 * Check if particle active
	 * */
	public boolean isParticleEffectActive() {
		return isParticleEffectActive;
	}

	/**
	 * Set particle active to draw or not
	 * */
	public void setParticleEffectActive(boolean isParticleEffectActive) {
		this.isParticleEffectActive = isParticleEffectActive;
	}
}
