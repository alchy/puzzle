package com.juego.Presentacion.Utilidad;

import com.badlogic.gdx.graphics.Pixmap;

public class Elemento {
	private int pixel;
	private Pixmap pixmapEmlemento;
	private int xInit;
	private int yInit;
	
	public Elemento() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	public int getxInit() {
		return xInit;
	}



	public void setxInit(int xInit) {
		this.xInit = xInit;
	}



	public int getyInit() {
		return yInit;
	}



	public void setyInit(int yInit) {
		this.yInit = yInit;
	}



	public int getPixel() {
		return pixel;
	}
	public void setPixel(int pixel) {
		this.pixel = pixel;
	}
	public Pixmap getPixmapEmlemento() {
		return pixmapEmlemento;
	}
	public void setPixmapEmlemento(Pixmap pixmapEmlemento) {
		this.pixmapEmlemento = pixmapEmlemento;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return pixel+"";
	}
	
}
