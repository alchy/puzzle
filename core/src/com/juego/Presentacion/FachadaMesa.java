package com.juego.Presentacion;

import java.util.ArrayList;
import java.util.Iterator;

import sun.reflect.generics.tree.BottomSignature;

import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.juego.Presentacion.Utilidad.ButtonLevel;
import com.juego.Presentacion.Utilidad.MenuCreator;
import com.juego.control.Recursos.AdministrarPreferencia;
import com.juego.control.Recursos.Nivel;
import com.juego.control.Recursos.Recursos2;
import com.juego.control.Rompecabeza.Mesa;

public class FachadaMesa {
	private Nivel nivel;
	private Mesa mesaRomcbz;
	private ArrayList<ButtonLevel> ListaPiezas;
	
	public FachadaMesa() {
		// TODO Auto-generated constructor stub
		nivel=cargarNivel();//leer le nivel guardado en las preferencias
		ListaPiezas=cargarPiezas(Recursos2.cargarNivelActual(nivel.getNombre()));
		int niv=nivel.getNivel();
		int tam=3;
		if(niv>=1 && niv<=4){
			tam=3;
		}else if (niv>=5 && niv<=8){
			tam=3;
		}
		else if (niv>=6 && niv<=12) {
			tam=3;
		}
		else{
			tam=3;
			
		}
		mesaRomcbz=new Mesa(tam);
		mesaRomcbz.iniciar();
		
	}
	
	public ArrayList<ButtonLevel> getPiezas(){
		return ListaPiezas;
		
	}



	public ArrayList<ButtonLevel> cargarPiezas(TextureRegion cargarNivelActual) {
		// TODO Auto-generated method stub
		int niv=nivel.getNivel();
		int tam;
		if(niv>=1 && niv<=4){
			tam=3;
		}else if (niv>=5 && niv<=8){
			tam=3;
		}
		else if (niv>=6 && niv<=12) {
			tam=3;
		}
		else{
			tam=3;
			
		}
		ArrayList<ButtonLevel>levels=new ArrayList<ButtonLevel>();
		TextureRegion regiones[][]=cargarNivelActual.split(cargarNivelActual.getRegionWidth()/tam, cargarNivelActual.getRegionHeight()/tam);
		int numeroFicha=1;
		for (int i =0; i<regiones.length; i++) {
			for (int j =0 ; j<regiones.length; j++) {
				ButtonLevel img=MenuCreator.createCustomLevelButton(regiones[i][j],regiones[i][j]);
				//img.setLevelNumber(numeroFicha, Recursos2.font2_1);
				img.setLevelNumberChange(numeroFicha);
				img.setSize(80,80);
				levels.add(img);
				if (numeroFicha<regiones.length*regiones.length-1)
				numeroFicha++;
				else numeroFicha=0;
				
			}
			
		}
		return levels;
	}



	public Nivel cargarNivel() {
		// TODO Auto-generated method stub
		nivel=AdministrarPreferencia.leerNivel();
		return nivel;
	}
	
	public ButtonLevel[][]getRepresentable(){
		int t=mesaRomcbz.getTam();
		ButtonLevel mesa[][]=new ButtonLevel[t][t];
		for (int i = 0; i <this.mesaRomcbz.getPiezas().length; i++) {
			for (int j = 0; j < this.mesaRomcbz.getPiezas().length; j++) {
				
				int elemento=this.mesaRomcbz.getPiezas()[i][j];
				for (int k = 0; k < ListaPiezas.size(); k++) {
					ButtonLevel buttonLevel=ListaPiezas.get(k);
					if(buttonLevel.getLevelNumber()==elemento){
						if (elemento==0)buttonLevel.setColor(.3f, .2f, .3f, .6f);;
						mesa[i][j]=buttonLevel;
						
					}
				}
						
				
			}
		}
		return mesa;
	}

	public Mesa getMesa() {
		// TODO Auto-generated method stub
		return this.mesaRomcbz;
	}
	
	

}
